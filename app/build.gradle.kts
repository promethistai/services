plugins {
    kotlin("multiplatform") version "1.5.30"
    kotlin("plugin.serialization") version "1.5.30"
    //id("kotlinx-serialization") version "1.5.21"
    id("maven-publish")
    application
}

group = "ai.promethist.services"

val ktorVersion = findProperty("ktorVersion") as String

kotlin {
    jvm {
        compilations.all {
            kotlinOptions.jvmTarget = JavaVersion.VERSION_11.toString()
        }
        testRuns["test"].executionTask.configure {
            useJUnit()
        }
        withJava()
    }
    js {

    }
    sourceSets {
        val commonMain by getting {
            dependencies {
                implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:1.3.0")
                implementation("org.jetbrains.kotlinx:kotlinx-datetime:0.3.0")
            }
        }
        val jsMain by getting
        val jvmMain by getting {
            dependencies {
                implementation("io.ktor:ktor-server-netty:$ktorVersion")
                implementation("io.ktor:ktor-html-builder:$ktorVersion")
                implementation("io.ktor:ktor-serialization:$ktorVersion")
                implementation("io.ktor:ktor-client-java:$ktorVersion")
                implementation("io.ktor:ktor-client-serialization:$ktorVersion")
                implementation("io.ktor:ktor-client-logging-jvm:$ktorVersion")
                implementation("org.jetbrains.kotlinx:kotlinx-html-jvm:0.7.2")
                implementation("org.slf4j:slf4j-simple:1.6.1")
            }
        }
    }
}

repositories {
    mavenCentral()
    maven { url = uri("https://maven.pkg.jetbrains.space/public/p/kotlinx-html/maven") }
}

publishing {
    repositories {
        maven {
            url = uri(findProperty("maven.repository.uri") as String)
            when (findProperty("maven.repository.auth") as String) {
                "gitlab" -> {
                    credentials(HttpHeaderCredentials::class) {
                        System.getenv("GITLAB_PRIVATE_TOKEN")?.let {
                            name = "Private-Token"
                            value = it
                        } ?: System.getenv("GITLAB_DEPLOY_TOKEN")?.let {
                            name = "Deploy-Token"
                            value = it
                        } ?: System.getenv("CI_JOB_TOKEN")?.let {
                            name = "Job-Token"
                            value = it
                        }
                    }
                    authentication {
                        create<HttpHeaderAuthentication>("header")
                    }
                }
            }
        }
    }
}

application {
    mainClass.set("ai.promethist.services.AppKt")
}

tasks.getByName<Jar>("jvmJar") {
    archiveAppendix.set("") // discard jvm appendix to make distribution bundle work
}
