package ai.promethist.services.changelog.model

import kotlinx.datetime.Instant
import kotlinx.serialization.Serializable

@Serializable
data class MergeRequest(
    val id: Int,
    val iid: Int,
    val title: String,
    val state: State,
    val description: String,
    val source_branch: String,
    val web_url: String,
    val author: User,
    val created_at: Instant,
    val updated_at: Instant?,
    val merged_by: User?,
    val merged_at: Instant?
) {
    enum class State { opened, merged, closed, locked }
}