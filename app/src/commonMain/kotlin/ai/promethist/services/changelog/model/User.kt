package ai.promethist.services.changelog.model

import kotlinx.serialization.Serializable

@Serializable
data class User(
    val id: Int,
    val name: String,
    val username: String,
    val state: String,
    val avatar_url: String,
    val web_url: String
)