package ai.promethist.services.changelog.model

import kotlinx.datetime.Instant
import kotlinx.serialization.Serializable

@Serializable
data class Tag(
    val name: String,
    val target: String,
    val commit: Commit
) {
    @Serializable
    data class Commit(
        val id: String,
        val short_id: String,
        val title: String,
        val created_at: Instant,
        val author_name: String,
        val author_email: String,
        val authored_date: Instant,
        val web_url: String
    )

    val title get() = commit.title
    val created get() = commit.authored_date
}
