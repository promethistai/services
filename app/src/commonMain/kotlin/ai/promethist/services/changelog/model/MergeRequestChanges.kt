package ai.promethist.services.changelog.model

import kotlinx.serialization.Serializable

@Serializable
data class MergeRequestChanges(
    val state: String,
    val draft: Boolean,
    val changes: List<Change>
) {
    @Serializable
    data class Change(
        val old_path: String,
        val new_path: String,
        val a_mode: String,
        val b_mode: String,
        val diff: String,
        val new_file: Boolean,
        val renamed_file: Boolean,
        val deleted_file: Boolean
    )
}
