package ai.promethist.services.changelog.model

import kotlinx.datetime.Instant
import kotlinx.serialization.Serializable

@Serializable
data class Changelog(
    val openItems: MutableList<Item> = mutableListOf(),
    val previewItems: MutableList<Item> = mutableListOf(),
    val releases: MutableList<Release> = mutableListOf(),
    val allItems: MutableList<Item> = mutableListOf()
) {
    @Serializable
    data class Release(
        val name: String,
        val target: String,
        val title: String,
        val created: Instant,
        val items: MutableList<Item> = mutableListOf()
    )

    @Serializable
    data class Item(
        val id: Int,
        val title: String,
        val description: String,
        val author: User,
        val created: Instant,
        val updated: Instant?,
        val merged: Instant?,
        val url: String,
        var changedPaths: List<String>,
        val git_branch: String,
        val gitlab_url: String,
        val tracker_url: String
    )
}
