package ai.promethist.type

import java.time.ZonedDateTime

typealias DateTime = ZonedDateTime

fun parseDateTime(text: CharSequence): DateTime = DateTime.parse(text)