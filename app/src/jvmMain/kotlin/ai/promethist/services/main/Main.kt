package ai.promethist.services.main

import io.ktor.application.*
import io.ktor.features.*
import io.ktor.html.*
import io.ktor.http.*
import io.ktor.http.content.*
import io.ktor.routing.*
import io.ktor.serialization.*
import kotlinx.html.*
import kotlinx.serialization.json.Json

fun Application.main() {

    install(CORS) {
        anyHost()
        method(HttpMethod.Options)
        header(HttpHeaders.Authorization)
        header(HttpHeaders.AccessControlAllowHeaders)
        header(HttpHeaders.ContentType)
        allowCredentials = true
        allowNonSimpleContentTypes = true
    }
    install(Compression)
    install(ContentNegotiation) {
        json(Json {
            prettyPrint = true
            isLenient = true
        })
    }

    routing {
        get("/") {
            call.respondHtml {
                head {
                    title("PromethistAI Services")
                }
                body {
                    div {
                        id = "root"
                        + "Hello from PromethistAI Services!"
                    }
                }
            }
        }
        static("/static") {
            resources()
        }
    }
}