package ai.promethist.services

import java.io.File
import java.io.FileReader
import java.util.*

class AppConfig {

    private val properties = Properties()

    fun load(file: File) = if (file.exists())
        properties.load(FileReader(file))
    else
        logger.warn("App config file $file not found")

    fun load(path: String) = javaClass.getResourceAsStream(path)?.let {
        properties.load(it)
    } ?: logger.warn("App config resource $path not found")

    operator fun get(name: String) = properties[name] as? String ?: error("App config property $name not found")
}