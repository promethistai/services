package ai.promethist.services

import io.ktor.client.*
import io.ktor.client.features.*
import io.ktor.client.features.json.*
import io.ktor.client.features.json.serializer.*
import io.ktor.http.*
import io.ktor.server.netty.*
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.io.File
import kotlinx.serialization.json.Json as KotlinJson

fun main(args: Array<String>) = EngineMain.main(args)

val config by lazy {
    AppConfig().apply {
        load("/app.properties")
        load(File("app.local.properties"))
    }
}

val client by lazy {
    HttpClient {
        install(JsonFeature) {
            serializer = KotlinxSerializer(KotlinJson {
                isLenient = true
                ignoreUnknownKeys = true
            })
        }
        defaultRequest {
            method = HttpMethod.Get
        }
    }
}

val logger: Logger by lazy {
    LoggerFactory.getLogger("ai.promethist.services")
}