package ai.promethist.services.changelog

import ai.promethist.services.changelog.service.Project
import io.ktor.application.*
import io.ktor.features.*
import io.ktor.http.*
import io.ktor.response.*
import io.ktor.routing.*

fun Application.changelog() {

    routing {
        get("/projects/{projectId}/changelog") {
            val projectId = call.parameters["projectId"] ?: error("Missing project ID")
            val branch = call.parameters["branch"] ?: "master"
            val withChangedPaths = listOf("true", "1").contains(call.parameters["withChangedPaths"])
            val response = Project.getChangelog(projectId, branch, withChangedPaths)
            call.respond(response)
        }
    }
}