package ai.promethist.services.changelog.service

import ai.promethist.services.logger
import ai.promethist.services.changelog.model.Changelog
import ai.promethist.services.changelog.model.MergeRequest
import kotlinx.coroutines.runBlocking
import kotlin.time.Duration
import kotlin.time.ExperimentalTime

object Project {

    @OptIn(ExperimentalTime::class)
    suspend fun getChangelog(projectId: String, branch: String, withChangedPaths: Boolean) = Changelog().apply {

        Gitlab.getTags(projectId).sortedByDescending { it.created }.forEach {
            // NOTE adding 2 seconds to created time is hack trying to address unclear problem when release tag is having slightly lower time than (last) issue merge (occurred at least with FST-477)
            releases.add(Changelog.Release(it.name, it.target, it.title, it.created.plus(Duration.seconds(2))))
        }

        Gitlab.getMergeRequests(projectId, MergeRequest.State.opened, branch).forEach {
            val item = createChangelogItemFromMergeRequest(it)
            if (withChangedPaths)
                item.addChangedPaths(projectId, it.iid)
            openItems.add(item)
        }

        Gitlab.getMergeRequests(projectId, MergeRequest.State.merged, branch).forEach {
            try {
                if (releases.isEmpty() || it.merged_at != null && releases[0].created < it.merged_at) {
                    val item = createChangelogItemFromMergeRequest(it)
                    if (withChangedPaths)
                        item.addChangedPaths(projectId, it.iid)
                    previewItems.add(item)
                } else for (i in 0 until releases.size - 1) {
                    if (releases[i].created >= it.merged_at!! && releases[i + 1].created < it.merged_at) {
                        val item = createChangelogItemFromMergeRequest(it, true)
                        if (withChangedPaths && i < 5)
                            item.addChangedPaths(projectId, it.iid)
                        releases[i].items.add(item)
                        break
                    }
                }
            } catch (e: Throwable) {
                logger.error(it.toString(), e)
            }
        }

        allItems.addAll(openItems)
        allItems.addAll(previewItems)
        allItems.addAll(releases.flatMap { it.items })
        logger.info("Project $projectId has ${openItems.size} item(s) open, ${previewItems.size} in preview, " + releases.sumOf { it.items.size } + " released " + releases.map { "${it.name}: ${it.items.size}" })
    }

    private suspend fun Changelog.Item.addChangedPaths(projectId: String, iid: Int) {
        val changedMap = mutableMapOf<String, Any>()
        Gitlab.getMergeRequestChanges(projectId, iid).changes.forEach { change ->
            var map = changedMap
            val names = change.new_path.split('/')
            var index = 0
            while (index < names.size - 1) {
                val name = names[index]
                if (listOf("src", "deploy").contains(name))
                    break
                map = map.getOrPut(name) { mutableMapOf<String, Any>() } as MutableMap<String, Any>
                index++
            }
            map[names[index]] = true
        }
        changedPaths = changedMap.flatten()
    }

    private fun MutableMap<String, Any>.flatten(maxDeep: Int = 5, path: String? = null, level: Int = 0): List<String> =
        mutableListOf<String>().also { list ->
            forEach { (name, value) ->
                val subPath = path?.let { "$it/$name" } ?: name
                if (value == true || level == maxDeep - 1) {
                    list.add(subPath)
                } else if (level < maxDeep - 1)
                    list.addAll((value as MutableMap<String, Any>).flatten(maxDeep, subPath, level + 1))
            }
        }

    private fun String.compLength(other: String): Int {
        var i = 0
        while (i < length && i < other.length) {
            if (this[i] != other[i])
                return i
            i++
        }
        return i
    }

    private fun createChangelogItemFromMergeRequest(mr: MergeRequest, released: Boolean = false): Changelog.Item {

        val id = mr.source_branch.substringAfter('-').substringBefore('-').toInt()
        val code = mr.source_branch.substringBefore('-').uppercase()
        val domain = "flowstorm.ai"
        return Changelog.Item(
            id,
            mr.title,
            mr.description,
            mr.author,
            mr.created_at,
            mr.updated_at,
            mr.merged_at,
            if (released)
                "https://app.$domain"
            else if (mr.state == MergeRequest.State.merged)
                "https://app-preview.$domain"
            else
                "https://app-$id.$domain",
            mutableListOf(),
            mr.source_branch,
            mr.web_url,
            "https://promethist.myjetbrains.com/youtrack/issue/$code-$id"
        )
    }

    @JvmStatic
    fun main(args: Array<String>): Unit = runBlocking {
        getChangelog("23512224", "master", true)
    }
}