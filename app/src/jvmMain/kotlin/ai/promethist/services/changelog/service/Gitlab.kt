package ai.promethist.services.changelog.service

import ai.promethist.services.client
import ai.promethist.services.config
import ai.promethist.services.logger
import ai.promethist.services.changelog.model.MergeRequest
import ai.promethist.services.changelog.model.MergeRequestChanges
import ai.promethist.services.changelog.model.Tag
import io.ktor.client.request.*

object Gitlab {

    private val url by lazy { config["gitlab.url"] }

    private suspend inline fun <reified R> get(uri: String) =
        client.get<R>(url + uri) {
            header("Authorization", "Bearer ${config["gitlab.token"]}")
        }

    suspend fun getMergeRequests(
        projectId: String,
        state: MergeRequest.State = MergeRequest.State.opened,
        targetBranch: String = "master",
        scope: String = "all",
        limit: Int = 1000
    ) =
        get<List<MergeRequest>>("/projects/$projectId/merge_requests?state=$state&scope=$scope&target_branch=$targetBranch&per_page=$limit").also {
            logger.info("Project $projectId has ${it.size} $state merge request(s)")
        }

    suspend fun getMergeRequestChanges(projectId: String, iid: Int) =
        get<MergeRequestChanges>("/projects/$projectId/merge_requests/$iid/changes").also {
            logger.info("Project $projectId merge request $iid contains ${it.changes.size} change(s)")
        }

    suspend fun getTags(projectId: String, limit: Int = 1000) =
        get<List<Tag>>("/projects/$projectId/repository/tags?per_page=$limit").also {
            logger.info("Project $projectId has ${it.size} tag(s)")
        }
}