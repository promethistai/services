rootProject.name = "promethistai-services"

arrayOf(
    "app",
).forEach {
    val projectPath = ":${rootProject.name}-${it.replace('/', '-')}"
    include(projectPath)
    project(projectPath).projectDir = file(it)
}
